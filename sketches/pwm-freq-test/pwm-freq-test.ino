#include <FreqMeasure.h>

const int numPins = 2;
const int outPins[] = {2, 3};
const int pwmFreq = 25000;     //  Hz
const int pwmDutys[] = {5, 95}; //  Percent

void setup() {
  // put your setup code here, to run once:
  for (int i = 0; i++; i < numPins) {
    pinMode(outPins[i], OUTPUT);
    analogWriteFrequency(outPins[i], pwmFreq);
    analogWrite(outPins[i], int(pwmDutys[i] * 255 / 100));
  }
  Serial.begin(57600);
  FreqMeasure.begin();
}


double sum = 0;
int count = 0;

void loop() {
  if (FreqMeasure.available()) {
    // average several reading together
    sum = sum + FreqMeasure.read();
    count = count + 1;
    if (count > 30) {
      float frequency = FreqMeasure.countToFrequency(sum / count);
      Serial.println(frequency);
      sum = 0;
      count = 0;
    }
  }
}
